
CREATE TABLE "transaction" (
 "id" UUID PRIMARY KEY ,
 "cash" NUMERIC,
 "uzcard" NUMERIC,
 "payme" NUMERIC,
 "click" NUMERIC,
 "humo" NUMERIC,
 "apelsin" NUMERIC,
  "total_price" NUMERIC ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);



CREATE TABLE "shift" (
  "id" UUID PRIMARY KEY ,
  "branch_id" UUID  NOT NULL,
  "staff_id" UUID  NOT NULL,
  "provider_id"  UUID  NOT NULL,
  "market_id"  UUID  NOT NULL,
  "status" varchar default 'opened',
  "transaction_id"  UUID ,
  "shift_id" varchar unique,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);


CREATE TABLE "sale_product" (
 "id" UUID PRIMARY KEY,
  "brand_id" UUID  NOT NULL,
  "product_id" UUID  NOT NULL,
  "category_id" UUID  NOT NULL,
  "bar_code" varchar unique,
  "remainder_id" UUID  NOT NULL,
  "count" int,
  "price" NUMERIC ,
  "total_price" NUMERIC ,
  "sale_id" UUID NOT NULL,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);





CREATE TABLE "sales" (
  "id" UUID PRIMARY KEY ,
  "branch_id" UUID  NOT NULL,
  "shift_id" UUID ,
  "market_id"  UUID  NOT NULL,
  "staff_id"  UUID  NOT NULL,
  "status" varchar default 'in_process',
  "payment_id" UUID ,
  "sale_id" varchar unique,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "payment" (
 "id" UUID PRIMARY KEY ,
 "cash" NUMERIC,
 "uzcard" NUMERIC,
 "payme" NUMERIC,
 "click" NUMERIC,
 "humo" NUMERIC,
 "apelsin" NUMERIC,
 "visa" NUMERIC,
 "currency" varchar,
 "exchange_sum" NUMERIC,
  "total_price" NUMERIC ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);