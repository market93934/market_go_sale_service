package storage

import (
	"context"

	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
)

type StorageI interface {
	CloseDB()
	Sales() SalesRepoI
	SalesProduct() SalesProductRepoI
	Shift() ShiftRepoI
	Transaction() TransactionRepoI
	Payment() PaymentRepoI
}

type SalesRepoI interface {
	Create(context.Context, *sales_service.CreateSales) (*sales_service.SalesPrimaryKey, error)
	GetByPKey(context.Context, *sales_service.SalesPrimaryKey) (*sales_service.Sales, error)
	GetAll(context.Context, *sales_service.GetListSalesRequest) (*sales_service.GetListSalesResponse, error)
	Update(context.Context, *sales_service.UpdateSales) (int64, error)
	Delete(context.Context, *sales_service.SalesPrimaryKey) (*sales_service.SalesEmpty, error)
}

type SalesProductRepoI interface {
	Create(context.Context, *sales_service.CreateSalesProduct) (*sales_service.SalesProductPrimaryKey, error)
	GetByPKey(context.Context, *sales_service.SalesProductPrimaryKey) (*sales_service.SalesProduct, error)
	GetAll(context.Context, *sales_service.GetListSalesProductRequest) (*sales_service.GetListSalesProductResponse, error)
	Update(context.Context, *sales_service.UpdateSalesProduct) (int64, error)
	Delete(context.Context, *sales_service.SalesProductPrimaryKey) (*sales_service.SalesProductEmpty, error)
}

type ShiftRepoI interface {
	Create(context.Context, *sales_service.CreateShift) (*sales_service.ShiftPrimaryKey, error)
	GetByPKey(context.Context, *sales_service.ShiftPrimaryKey) (*sales_service.Shift, error)
	GetAll(context.Context, *sales_service.GetListShiftRequest) (*sales_service.GetListShiftResponse, error)
	Update(context.Context, *sales_service.UpdateShift) (int64, error)
	Delete(context.Context, *sales_service.ShiftPrimaryKey) (*sales_service.ShiftEmpty, error)
}

type TransactionRepoI interface {
	Create(context.Context, *sales_service.CreateTransaction) (*sales_service.TransactionPrimaryKey, error)
	GetByPKey(context.Context, *sales_service.TransactionPrimaryKey) (*sales_service.Transaction, error)
	GetAll(context.Context, *sales_service.GetListTransactionRequest) (*sales_service.GetListTransactionResponse, error)
	Update(context.Context, *sales_service.UpdateTransaction) (int64, error)
	Delete(context.Context, *sales_service.TransactionPrimaryKey) (*sales_service.TransactionEmpty, error)
}

type PaymentRepoI interface {
	Create(context.Context, *sales_service.CreatePayment) (*sales_service.PaymentPrimaryKey, error)
	GetByPKey(context.Context, *sales_service.PaymentPrimaryKey) (*sales_service.Payment, error)
	GetAll(context.Context, *sales_service.GetListPaymentRequest) (*sales_service.GetListPaymentResponse, error)
	Update(context.Context, *sales_service.UpdatePayment) (int64, error)
	Delete(context.Context, *sales_service.PaymentPrimaryKey) (*sales_service.PaymentEmpty, error)
}
