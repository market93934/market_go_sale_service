package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/pkg/helper"
)

type salesRepo struct {
	db *pgxpool.Pool
}

func NewSalesRepo(db *pgxpool.Pool) *salesRepo {
	return &salesRepo{
		db: db,
	}
}

func (c *salesRepo) Create(ctx context.Context, req *sales_service.CreateSales) (resp *sales_service.SalesPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "sales" (
			id,
			branch_id,
			shift_id,
			market_id,
			staff_id,
			payment_id,
			sale_id
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.ShiftId,
		req.MarketId,
		req.StaffId,
		req.PaymentId,
		"1",
	)
	if err != nil {
		return nil, err
	}

	return &sales_service.SalesPrimaryKey{Id: id.String()}, nil

}

func (c *salesRepo) GetByPKey(ctx context.Context, req *sales_service.SalesPrimaryKey) (resp *sales_service.Sales, err error) {
	query := `
		SELECT
			id,
			branch_id,
			shift_id,
			market_id,
			staff_id,
			status,
			payment_id,
			sale_id,
			created_at,
			updated_at
		FROM "sales"
		WHERE id = $1
	`

	var (
		id         sql.NullString
		branch_id  sql.NullString
		shift_id   sql.NullString
		market_id  sql.NullString
		staff_id   sql.NullString
		status     sql.NullString
		payment_id sql.NullString
		sale_id    sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&shift_id,
		&market_id,
		&staff_id,
		&status,
		&payment_id,
		&sale_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sales_service.Sales{
		Id:        id.String,
		BranchId:  branch_id.String,
		ShiftId:   shift_id.String,
		MarketId:  market_id.String,
		StaffId:   staff_id.String,
		Status:    status.String,
		PaymentId: payment_id.String,
		SaleId:    sale_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *salesRepo) GetAll(ctx context.Context, req *sales_service.GetListSalesRequest) (resp *sales_service.GetListSalesResponse, err error) {

	resp = &sales_service.GetListSalesResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			shift_id,
			market_id,
			staff_id,
			status,
			payment_id,
			sale_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sales"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if len(req.Search) > 0 {
		filter += ` AND sale_id ILIKE '%' || '` + req.Search + `' || '%'`
	}
	if req.SearchShiftId != "" {
		filter += ` AND shift_id ILIKE '%' || '` + req.SearchShiftId + `' || '%'`
	}
	if req.SearchStaffId != "" {
		filter += ` AND staff_id ILIKE '%' || '` + req.SearchStaffId + `' || '%'`
	}
	if req.SearchBranchId != "" {
		filter += ` AND branch_id ILIKE '%' || '` + req.SearchBranchId + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			branch_id  sql.NullString
			shift_id   sql.NullString
			market_id  sql.NullString
			staff_id   sql.NullString
			status     sql.NullString
			payment_id sql.NullString
			sale_id    sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&shift_id,
			&market_id,
			&staff_id,
			&status,
			&payment_id,
			&sale_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Saless = append(resp.Saless, &sales_service.Sales{
			Id:        id.String,
			BranchId:  branch_id.String,
			ShiftId:   shift_id.String,
			MarketId:  market_id.String,
			StaffId:   staff_id.String,
			Status:    status.String,
			PaymentId: payment_id.String,
			SaleId:    sale_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *salesRepo) Update(ctx context.Context, req *sales_service.UpdateSales) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "sales"
			SET
				branch_id = :branch_id,
				shift_id = :shift_id,
				market_id = :market_id,
				staff_id = :staff_id,
				status = :status,
				payment_id = :payment_id,
				sale_id =:sale_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"branch_id":  req.GetBranchId(),
		"shift_id":   req.GetShiftId(),
		"market_id":  req.GetMarketId(),
		"staff_id":   req.GetStaffId(),
		"status":     req.GetStatus(),
		"payment_id": req.GetPaymentId(),
		"sale_id":    req.GetSaleId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *salesRepo) Delete(ctx context.Context, req *sales_service.SalesPrimaryKey) (*sales_service.SalesEmpty, error) {

	query := `DELETE FROM "sales" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &sales_service.SalesEmpty{}, nil
}
