package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/pkg/helper"
)

type paymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) *paymentRepo {
	return &paymentRepo{
		db: db,
	}
}

func (c *paymentRepo) Create(ctx context.Context, req *sales_service.CreatePayment) (resp *sales_service.PaymentPrimaryKey, err error) {
	var (
		id                  = uuid.New()
		total_price float64 = 0
	)
	query := `INSERT INTO "payment" (
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			visa,
			currency,
			exchange_sum,
			total_price
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
	`
	total_price = req.Cash + req.Uzcard + req.Payme + req.Click + req.Humo + req.Apelsin + req.Visa
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		req.Visa,
		req.Currency,
		req.ExchangeSum,
		total_price,
	)
	if err != nil {
		return nil, err
	}

	return &sales_service.PaymentPrimaryKey{Id: id.String()}, nil

}

func (c *paymentRepo) GetByPKey(ctx context.Context, req *sales_service.PaymentPrimaryKey) (resp *sales_service.Payment, err error) {
	query := `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			visa,
			currency,
			exchange_sum,
			total_price,
			created_at,
			updated_at
		FROM "payment"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		cash         float64
		uzcard       float64
		payme        float64
		click        float64
		humo         float64
		apelsin      float64
		visa         float64
		currency     sql.NullString
		exchange_sum float64
		total_price  float64
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&visa,
		&currency,
		&exchange_sum,
		&total_price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sales_service.Payment{
		Id:          id.String,
		Cash:        cash,
		Uzcard:      uzcard,
		Payme:       payme,
		Click:       click,
		Humo:        humo,
		Apelsin:     apelsin,
		Visa:        visa,
		Currency:    currency.String,
		ExchangeSum: exchange_sum,
		TotalPrice:  total_price,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *paymentRepo) GetAll(ctx context.Context, req *sales_service.GetListPaymentRequest) (resp *sales_service.GetListPaymentResponse, err error) {

	resp = &sales_service.GetListPaymentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			visa,
			currency,
			exchange_sum,
			total_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "payment"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			cash         float64
			uzcard       float64
			payme        float64
			click        float64
			humo         float64
			apelsin      float64
			visa         float64
			currency     sql.NullString
			exchange_sum float64
			total_price  float64
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&visa,
			&currency,
			&exchange_sum,
			&total_price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Payments = append(resp.Payments, &sales_service.Payment{
			Id:          id.String,
			Cash:        cash,
			Uzcard:      uzcard,
			Payme:       payme,
			Click:       click,
			Humo:        humo,
			Apelsin:     apelsin,
			Visa:        visa,
			Currency:    currency.String,
			ExchangeSum: exchange_sum,
			TotalPrice:  total_price,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *paymentRepo) Update(ctx context.Context, req *sales_service.UpdatePayment) (rowsAffected int64, err error) {

	var (
		query       string
		params      map[string]interface{}
		total_price float64 = 0
	)

	query = `
			UPDATE
			    "payment"
			SET
				cash  =:cash,
				uzcard  =:uzcard,
				payme  =:payme,
				click  =:click,
				humo  =:humo,
				apelsin  =:apelsin,
				visa  =:visa,
				currency =:currency,
				exchange_sum =:exchange_sum,
				total_price  =:total_price,
				updated_at = now()
			WHERE
				id = :id`

	total_price = req.Cash + req.Uzcard + req.Payme + req.Click + req.Humo + req.Apelsin + req.Visa

	params = map[string]interface{}{
		"id":           req.GetId(),
		"cash":         req.GetCash(),
		"uzcard":       req.GetUzcard(),
		"payme":        req.GetPayme(),
		"click":        req.GetCash(),
		"humo":         req.GetHumo(),
		"apelsin":      req.GetApelsin(),
		"visa":         req.GetVisa(),
		"currency":     req.GetCurrency(),
		"exchange_sum": req.GetExchangeSum(),
		"total_price":  total_price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *paymentRepo) Delete(ctx context.Context, req *sales_service.PaymentPrimaryKey) (*sales_service.PaymentEmpty, error) {

	query := `DELETE FROM "payment" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &sales_service.PaymentEmpty{}, nil
}
