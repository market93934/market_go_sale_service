package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/pkg/helper"
)

type shiftRepo struct {
	db *pgxpool.Pool
}

func NewShiftRepo(db *pgxpool.Pool) *shiftRepo {
	return &shiftRepo{
		db: db,
	}
}

func (c *shiftRepo) Create(ctx context.Context, req *sales_service.CreateShift) (resp *sales_service.ShiftPrimaryKey, err error) {
	count, err := c.GetAll(ctx, &sales_service.GetListShiftRequest{})
	if err != nil {
		return nil, err
	}

	var (
		id             = uuid.New()
		transaction_id = uuid.New()
		sale_id        = helper.GenerateString("S", int(count.Count)+1)
	)
	query := `INSERT INTO "shift" (
			id,
			branch_id,
			provider_id,
			market_id,
			transaction_id,
			shift_id,
			staff_id
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.ProviderId,
		req.MarketId,
		transaction_id.String(),
		sale_id,
		req.StaffId,
	)
	if err != nil {
		return nil, err
	}

	return &sales_service.ShiftPrimaryKey{Id: id.String()}, nil

}

func (c *shiftRepo) GetByPKey(ctx context.Context, req *sales_service.ShiftPrimaryKey) (resp *sales_service.Shift, err error) {
	query := `
		SELECT
			id,
			branch_id,
			provider_id,
			market_id,
			status,
			transaction_id,
			shift_id,
			staff_id,
			created_at,
			updated_at
		FROM "shift"
		WHERE id = $1
	`

	var (
		id             sql.NullString
		branch_id      sql.NullString
		provider_id    sql.NullString
		market_id      sql.NullString
		status         sql.NullString
		transaction_id sql.NullString
		shift_id       sql.NullString
		staff_id       sql.NullString
		createdAt      sql.NullString
		updatedAt      sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&provider_id,
		&market_id,
		&status,
		&transaction_id,
		&shift_id,
		&staff_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sales_service.Shift{
		Id:            id.String,
		BranchId:      branch_id.String,
		ProviderId:    provider_id.String,
		MarketId:      market_id.String,
		Status:        status.String,
		TransactionId: transaction_id.String,
		ShiftId:       shift_id.String,
		StaffId:       staff_id.String,
		CreatedAt:     createdAt.String,
		UpdatedAt:     updatedAt.String,
	}

	return
}

func (c *shiftRepo) GetAll(ctx context.Context, req *sales_service.GetListShiftRequest) (resp *sales_service.GetListShiftResponse, err error) {

	resp = &sales_service.GetListShiftResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			provider_id,
			market_id,
			status,
			transaction_id,
			shift_id,
			staff_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "shift"
	`
	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchShiftId != "" {
		filter += ` AND shift_id ILIKE '%' || '` + req.SearchShiftId + `' || '%'`
	}
	if req.SearchBranchId != "" {
		filter += ` AND branch_id ILIKE '%' || '` + req.SearchBranchId + `' || '%'`
	}
	if req.SearchStaffId != "" {
		filter += ` AND staff_id ILIKE '%' || '` + req.SearchStaffId + `' || '%'`
	}
	if req.SearchMarketId != "" {
		filter += ` AND market_id ILIKE '%' || '` + req.SearchMarketId + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			branch_id      sql.NullString
			provider_id    sql.NullString
			market_id      sql.NullString
			status         sql.NullString
			transaction_id sql.NullString
			shift_id       sql.NullString
			staff_id       sql.NullString
			createdAt      sql.NullString
			updatedAt      sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&provider_id,
			&market_id,
			&status,
			&transaction_id,
			&shift_id,
			&staff_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Shifts = append(resp.Shifts, &sales_service.Shift{
			Id:            id.String,
			BranchId:      branch_id.String,
			ProviderId:    provider_id.String,
			MarketId:      market_id.String,
			Status:        status.String,
			TransactionId: transaction_id.String,
			ShiftId:       shift_id.String,
			StaffId:       staff_id.String,
			CreatedAt:     createdAt.String,
			UpdatedAt:     updatedAt.String,
		})
	}

	return
}

func (c *shiftRepo) Update(ctx context.Context, req *sales_service.UpdateShift) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "shift"
			SET
				branch_id = :branch_id,
				provider_id = :provider_id,
				market_id = :market_id,
				status = :status,
				staff_id =:staff_id,
				transaction_id =:transaction_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":             req.GetId(),
		"branch_id":      req.GetBranchId(),
		"provider_id":    req.GetProviderId(),
		"market_id":      req.GetMarketId(),
		"status":         req.GetStatus(),
		"staff_id":       req.GetStaffId(),
		"transaction_id": req.GetTransactionId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *shiftRepo) Delete(ctx context.Context, req *sales_service.ShiftPrimaryKey) (*sales_service.ShiftEmpty, error) {

	query := `DELETE FROM "shift" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &sales_service.ShiftEmpty{}, nil
}
