package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/pkg/helper"
)

type transactionRepo struct {
	db *pgxpool.Pool
}

func NewTransactionRepo(db *pgxpool.Pool) *transactionRepo {
	return &transactionRepo{
		db: db,
	}
}

func (c *transactionRepo) Create(ctx context.Context, req *sales_service.CreateTransaction) (resp *sales_service.TransactionPrimaryKey, err error) {
	var (
		id                  = uuid.New()
		total_price float64 = 0
	)
	query := `INSERT INTO "transaction" (
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_price
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`

	total_price = req.Cash + req.Uzcard + req.Payme + req.Click + req.Humo + req.Apelsin

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		total_price,
	)
	if err != nil {
		return nil, err
	}

	return &sales_service.TransactionPrimaryKey{Id: id.String()}, nil

}

func (c *transactionRepo) GetByPKey(ctx context.Context, req *sales_service.TransactionPrimaryKey) (resp *sales_service.Transaction, err error) {
	query := `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_price,
			created_at,
			updated_at
		FROM "transaction"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		cash        float64
		uzcard      float64
		payme       float64
		click       float64
		humo        float64
		apelsin     float64
		total_price float64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&total_price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sales_service.Transaction{
		Id:         id.String,
		Cash:       cash,
		Uzcard:     uzcard,
		Payme:      payme,
		Click:      click,
		Humo:       humo,
		Apelsin:    apelsin,
		TotalPrice: total_price,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *transactionRepo) GetAll(ctx context.Context, req *sales_service.GetListTransactionRequest) (resp *sales_service.GetListTransactionResponse, err error) {

	resp = &sales_service.GetListTransactionResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "transaction"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			cash        float64
			uzcard      float64
			payme       float64
			click       float64
			humo        float64
			apelsin     float64
			total_price float64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&total_price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Transactions = append(resp.Transactions, &sales_service.Transaction{
			Id:         id.String,
			Cash:       cash,
			Uzcard:     uzcard,
			Payme:      payme,
			Click:      click,
			Humo:       humo,
			Apelsin:    apelsin,
			TotalPrice: total_price,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *transactionRepo) Update(ctx context.Context, req *sales_service.UpdateTransaction) (rowsAffected int64, err error) {

	var (
		query       string
		params      map[string]interface{}
		total_price float64 = 0
	)

	query = `
			UPDATE
			    "transaction"
			SET
				cash  =:cash,
				uzcard  =:uzcard,
				payme  =:payme,
				click  =:click,
				humo  =:humo,
				apelsin  =:apelsin,
				total_price  =:total_price,
				updated_at = now()
			WHERE
				id = :id`
	total_price = req.Cash + req.Uzcard + req.Payme + req.Click + req.Humo + req.Apelsin

	params = map[string]interface{}{
		"id":          req.GetId(),
		"cash":        req.GetCash(),
		"uzcard":      req.GetUzcard(),
		"payme":       req.GetPayme(),
		"click":       req.GetClick(),
		"humo":        req.GetHumo(),
		"apelsin":     req.GetApelsin(),
		"total_price": total_price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *transactionRepo) Delete(ctx context.Context, req *sales_service.TransactionPrimaryKey) (*sales_service.TransactionEmpty, error) {

	query := `DELETE FROM "transaction" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &sales_service.TransactionEmpty{}, nil
}
