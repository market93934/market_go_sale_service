package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/pkg/helper"
)

type salesProductRepo struct {
	db *pgxpool.Pool
}

func NewSalesProductRepo(db *pgxpool.Pool) *salesProductRepo {
	return &salesProductRepo{
		db: db,
	}
}

func (c *salesProductRepo) Create(ctx context.Context, req *sales_service.CreateSalesProduct) (resp *sales_service.SalesProductPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "sale_product" (
			id,
			brand_id,
			product_id,
			category_id,
			bar_code,
			remainder_id,
			count,
			price,
			total_price,
			sale_id
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	`
	total_price := req.Count * int64(req.Price)
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BrandId,
		req.ProductId,
		req.CategoryId,
		req.BarCode,
		req.RemainderId,
		req.Count,
		req.Price,
		float64(total_price),
		req.SaleId,
	)
	if err != nil {
		return nil, err
	}

	return &sales_service.SalesProductPrimaryKey{Id: id.String()}, nil

}

func (c *salesProductRepo) GetByPKey(ctx context.Context, req *sales_service.SalesProductPrimaryKey) (resp *sales_service.SalesProduct, err error) {
	query := `
		SELECT
			id,
			brand_id,
			product_id,
			category_id,
			bar_code,
			remainder_id,
			count,
			price,
			total_price,
			sale_id,
			created_at,
			updated_at
		FROM "sale_product"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		brand_id     sql.NullString
		product_id   sql.NullString
		category_id  sql.NullString
		bar_code     sql.NullString
		remainder_id sql.NullString
		count        int
		price        float64
		total_price  float64
		sale_id      sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&brand_id,
		&product_id,
		&category_id,
		&bar_code,
		&remainder_id,
		&count,
		&price,
		&total_price,
		&sale_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sales_service.SalesProduct{
		Id:          id.String,
		BrandId:     brand_id.String,
		ProductId:   product_id.String,
		CategoryId:  category_id.String,
		BarCode:     bar_code.String,
		RemainderId: remainder_id.String,
		Count:       int64(count),
		Price:       price,
		TotalPrice:  total_price,
		SaleId:      sale_id.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *salesProductRepo) GetAll(ctx context.Context, req *sales_service.GetListSalesProductRequest) (resp *sales_service.GetListSalesProductResponse, err error) {

	resp = &sales_service.GetListSalesProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand_id,
			product_id,
			category_id,
			bar_code,
			remainder_id,
			count,
			price,
			total_price,
			sale_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale_product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if len(req.BarCode) > 0 {
		filter += ` AND bar_code ILIKE '%' || '` + req.BarCode + `' || '%'`
	}
	if req.SearchBrandId !=""{
		filter += ` AND brand_id ILIKE '%' || '` +req.SearchBrandId + `' || '%'`
	}
	if req.SearchCategoryId != ""{
		filter += ` AND cactegory_id ILIKE '%' || '`+ req.SearchCategoryId + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			brand_id     sql.NullString
			product_id   sql.NullString
			category_id  sql.NullString
			bar_code     sql.NullString
			remainder_id sql.NullString
			count        int
			price        float64
			total_price  float64
			sale_id      sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&brand_id,
			&product_id,
			&category_id,
			&bar_code,
			&remainder_id,
			&count,
			&price,
			&total_price,
			&sale_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.SalesProducts = append(resp.SalesProducts, &sales_service.SalesProduct{
			Id:          id.String,
			BrandId:     brand_id.String,
			ProductId:   product_id.String,
			CategoryId:  category_id.String,
			BarCode:     bar_code.String,
			RemainderId: remainder_id.String,
			Count:       int64(count),
			Price:       price,
			TotalPrice:  total_price,
			SaleId:      sale_id.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *salesProductRepo) Update(ctx context.Context, req *sales_service.UpdateSalesProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "sale_product"
			SET
				brand_id = :brand_id,
				product_id = :product_id,
				category_id =:category_id,
				bar_code = :bar_code,
				remainder_id = :remainder_id,
				count = :count,
				price = :price,
				total_price = :total_price,
				sale_id =:sale_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"brand_id":     req.GetBrandId(),
		"product_id":   req.GetProductId(),
		"category_id":  req.GetCategoryId(),
		"bar_code":     req.GetBarCode(),
		"remainder_id": req.GetRemainderId(),
		"count":        req.GetCount(),
		"price":        req.GetPrice(),
		"total_price":  float64(req.Count * int64(req.Price)),
		"sale_id":      req.GetSaleId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *salesProductRepo) Delete(ctx context.Context, req *sales_service.SalesProductPrimaryKey) (*sales_service.SalesProductEmpty, error) {

	query := `DELETE FROM "sale_product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &sales_service.SalesProductEmpty{}, nil
}
