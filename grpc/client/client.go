package client

import (
	"gitlab.com/market93934/market_go_sale_service/config"
	"gitlab.com/market93934/market_go_sale_service/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	BranchService() user_service.BranchServiceClient
}

type grpcClients struct {
	branchService user_service.BranchServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	connBranchService, err := grpc.Dial(
		cfg.USERServiceHost+cfg.USERGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	return &grpcClients{
		branchService: user_service.NewBranchServiceClient(connBranchService),
	}, nil
}
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}
