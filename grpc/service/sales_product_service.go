package service

import (
	"context"

	"gitlab.com/market93934/market_go_sale_service/config"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/grpc/client"
	"gitlab.com/market93934/market_go_sale_service/pkg/logger"
	"gitlab.com/market93934/market_go_sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type salesProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sales_service.UnimplementedSalesProductServiceServer
}

func NewsalesProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *salesProductService {
	return &salesProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *salesProductService) Create(ctx context.Context, req *sales_service.CreateSalesProduct) (resp *sales_service.SalesProduct, err error) {

	i.log.Info("---CreateSalesProduct------>", logger.Any("req", req))

	pKey, err := i.strg.SalesProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSalesProduct->SalesProduct->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SalesProduct().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySalesProduct->SalesProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *salesProductService) GetByID(ctx context.Context, req *sales_service.SalesProductPrimaryKey) (resp *sales_service.SalesProduct, err error) {

	i.log.Info("---GetSalesProductByID------>", logger.Any("req", req))

	resp, err = i.strg.SalesProduct().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSalesProductByID->SalesProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *salesProductService) GetList(ctx context.Context, req *sales_service.GetListSalesProductRequest) (*sales_service.GetListSalesProductResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.SalesProduct().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListSalesProduct->SalesProduct->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *salesProductService) Update(ctx context.Context, req *sales_service.UpdateSalesProduct) (resp *sales_service.SalesProduct, err error) {
	i.log.Info("---UpdateSalesProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SalesProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSalesProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SalesProduct().GetByPKey(ctx, &sales_service.SalesProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSalesProduct->SalesProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *salesProductService) Delete(ctx context.Context, req *sales_service.SalesProductPrimaryKey) (resp *sales_service.SalesProductEmpty, err error) {

	i.log.Info("---DeleteSalesProduct------>", logger.Any("req", req))

	resp, err = i.strg.SalesProduct().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSalesProduct->SalesProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
