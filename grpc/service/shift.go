package service

import (
	"context"
	"fmt"

	"gitlab.com/market93934/market_go_sale_service/config"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/grpc/client"
	"gitlab.com/market93934/market_go_sale_service/pkg/logger"
	"gitlab.com/market93934/market_go_sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type shiftService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sales_service.UnimplementedShiftServiceServer
}

func NewshiftService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *shiftService {
	return &shiftService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *shiftService) Create(ctx context.Context, req *sales_service.CreateShift) (resp *sales_service.Shift, err error) {
	isOpen := false
	i.log.Info("---CreateShift------>", logger.Any("req", req))
	shifts, err := i.strg.Shift().GetAll(ctx, &sales_service.GetListShiftRequest{
		Offset: 0,
		Limit:  0,
	})
	if err != nil {
		i.log.Error("!!!CreateShift->Shift->GetAll--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	fmt.Println(shifts)
	for _, val := range shifts.Shifts {
		if val.BranchId == req.BranchId {
			isOpen = true
		}
	}
	if isOpen == false {
		pKey, err := i.strg.Shift().Create(ctx, req)
		if err != nil {
			i.log.Error("!!!CreateShift->Shift->Create--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		resp, err = i.strg.Shift().GetByPKey(ctx, pKey)
		if err != nil {
			i.log.Error("!!!GetByPKeyShift->Shift->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		_, err = i.strg.Shift().GetByPKey(ctx, &sales_service.ShiftPrimaryKey{
			Id: resp.Id,
		})
		if err != nil {
			i.log.Error("!!!UpdateShidt->Shift->Create--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}


	} else {
		i.log.Info("In this branch already open a shift")
		return
	}
	return
}
func (i *shiftService) GetByID(ctx context.Context, req *sales_service.ShiftPrimaryKey) (resp *sales_service.Shift, err error) {

	i.log.Info("---GetShiftByID------>", logger.Any("req", req))

	resp, err = i.strg.Shift().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShiftByID->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *shiftService) GetList(ctx context.Context, req *sales_service.GetListShiftRequest) (*sales_service.GetListShiftResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Shift().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListShift->Shift->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *shiftService) Update(ctx context.Context, req *sales_service.UpdateShift) (resp *sales_service.Shift, err error) {
	i.log.Info("---UpdateShift------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Shift().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateShift--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Shift().GetByPKey(ctx, &sales_service.ShiftPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShift->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}
	if resp.Status == "Closed" {
		i.log.Info("Shift Was Successfully Closed !! ")
	}
	return resp, err
}

func (i *shiftService) Delete(ctx context.Context, req *sales_service.ShiftPrimaryKey) (resp *sales_service.ShiftEmpty, err error) {

	i.log.Info("---DeleteShift------>", logger.Any("req", req))

	resp, err = i.strg.Shift().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteShift->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
