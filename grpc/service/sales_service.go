package service

import (
	"context"
	"fmt"

	"gitlab.com/market93934/market_go_sale_service/config"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/grpc/client"
	"gitlab.com/market93934/market_go_sale_service/pkg/logger"
	"gitlab.com/market93934/market_go_sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type salesService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sales_service.UnimplementedSalesServiceServer
}

func NewsalesService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *salesService {
	return &salesService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *salesService) Create(ctx context.Context, req *sales_service.CreateSales) (resp *sales_service.Sales, err error) {

	i.log.Info("---CreateSales------>", logger.Any("req", req))

	shift, err := i.strg.Shift().GetByPKey(ctx, &sales_service.ShiftPrimaryKey{
		Id: req.ShiftId,
	})
	if err != nil {
		i.log.Error("!!!CreateSales->Sales->GetShidtById--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	fmt.Println(shift.Status)
	if shift.Status == "opened" {
		pKey, err := i.strg.Sales().Create(ctx, req)
		if err != nil {
			i.log.Error("!!!CreateSales->Sales->Create--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		resp, err = i.strg.Sales().GetByPKey(ctx, pKey)
		if err != nil {
			i.log.Error("!!!GetByPKeySales->Sales->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	} else {
		fmt.Println(shift.Status)
		i.log.Info("!!!First Open Shift")
	}
	return

}
func (i *salesService) GetByID(ctx context.Context, req *sales_service.SalesPrimaryKey) (resp *sales_service.Sales, err error) {

	i.log.Info("---GetSalesByID------>", logger.Any("req", req))

	resp, err = i.strg.Sales().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSalesByID->Sales->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *salesService) GetList(ctx context.Context, req *sales_service.GetListSalesRequest) (*sales_service.GetListSalesResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Sales().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListSales->Sales->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *salesService) Update(ctx context.Context, req *sales_service.UpdateSales) (resp *sales_service.Sales, err error) {
	i.log.Info("---UpdateSales------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Sales().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSales--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Sales().GetByPKey(ctx, &sales_service.SalesPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSales->Sales->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *salesService) Delete(ctx context.Context, req *sales_service.SalesPrimaryKey) (resp *sales_service.SalesEmpty, err error) {

	i.log.Info("---DeleteSales------>", logger.Any("req", req))

	resp, err = i.strg.Sales().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSales->Sales->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
