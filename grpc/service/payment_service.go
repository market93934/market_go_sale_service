package service

import (
	"context"

	"gitlab.com/market93934/market_go_sale_service/config"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/grpc/client"
	"gitlab.com/market93934/market_go_sale_service/pkg/logger"
	"gitlab.com/market93934/market_go_sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type paymentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sales_service.UnimplementedPaymentServiceServer
}

func NewpaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *paymentService {
	return &paymentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *paymentService) Create(ctx context.Context, req *sales_service.CreatePayment) (resp *sales_service.Payment, err error) {

	i.log.Info("---CreatePayment------>", logger.Any("req", req))

	pKey, err := i.strg.Payment().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreatePayment->Payment->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Payment().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *paymentService) GetByID(ctx context.Context, req *sales_service.PaymentPrimaryKey) (resp *sales_service.Payment, err error) {

	i.log.Info("---GetPaymentByID------>", logger.Any("req", req))

	resp, err = i.strg.Payment().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPaymentByID->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *paymentService) GetList(ctx context.Context, req *sales_service.GetListPaymentRequest) (*sales_service.GetListPaymentResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Payment().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListPayment->Payment->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *paymentService) Update(ctx context.Context, req *sales_service.UpdatePayment) (resp *sales_service.Payment, err error) {
	i.log.Info("---UpdatePayment------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Payment().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdatePayment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Payment().GetByPKey(ctx, &sales_service.PaymentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *paymentService) Delete(ctx context.Context, req *sales_service.PaymentPrimaryKey) (resp *sales_service.PaymentEmpty, err error) {

	i.log.Info("---DeletePayment------>", logger.Any("req", req))

	resp, err = i.strg.Payment().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeletePayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
