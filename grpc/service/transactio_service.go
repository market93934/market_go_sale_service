package service

import (
	"context"

	"gitlab.com/market93934/market_go_sale_service/config"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/grpc/client"
	"gitlab.com/market93934/market_go_sale_service/pkg/logger"
	"gitlab.com/market93934/market_go_sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type transactionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sales_service.UnimplementedTransactionServiceServer
}

func NewtransactionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *transactionService {
	return &transactionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *transactionService) Create(ctx context.Context, req *sales_service.CreateTransaction) (resp *sales_service.Transaction, err error) {

	i.log.Info("---CreateTransaction------>", logger.Any("req", req))

	pKey, err := i.strg.Transaction().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateTransaction->Transaction->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Transaction().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *transactionService) GetByID(ctx context.Context, req *sales_service.TransactionPrimaryKey) (resp *sales_service.Transaction, err error) {

	i.log.Info("---GetTransactionByID------>", logger.Any("req", req))

	resp, err = i.strg.Transaction().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTransactionByID->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *transactionService) GetList(ctx context.Context, req *sales_service.GetListTransactionRequest) (*sales_service.GetListTransactionResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Transaction().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListTransaction->Transaction->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *transactionService) Update(ctx context.Context, req *sales_service.UpdateTransaction) (resp *sales_service.Transaction, err error) {
	i.log.Info("---UpdateTransaction------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Transaction().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateTransaction--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Transaction().GetByPKey(ctx, &sales_service.TransactionPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *transactionService) Delete(ctx context.Context, req *sales_service.TransactionPrimaryKey) (resp *sales_service.TransactionEmpty, err error) {

	i.log.Info("---DeleteTransaction------>", logger.Any("req", req))

	resp, err = i.strg.Transaction().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
