package grpc

import (
	"gitlab.com/market93934/market_go_sale_service/config"
	"gitlab.com/market93934/market_go_sale_service/genproto/sales_service"
	"gitlab.com/market93934/market_go_sale_service/grpc/client"
	"gitlab.com/market93934/market_go_sale_service/grpc/service"
	"gitlab.com/market93934/market_go_sale_service/pkg/logger"
	"gitlab.com/market93934/market_go_sale_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	sales_service.RegisterSalesServiceServer(grpcServer, service.NewsalesService(cfg, log, strg, srvc))
	sales_service.RegisterSalesProductServiceServer(grpcServer, service.NewsalesProductService(cfg, log, strg, srvc))
	sales_service.RegisterPaymentServiceServer(grpcServer, service.NewpaymentService(cfg, log, strg, srvc))
	sales_service.RegisterShiftServiceServer(grpcServer, service.NewshiftService(cfg, log, strg, srvc))
	sales_service.RegisterTransactionServiceServer(grpcServer, service.NewtransactionService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
